/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.bean;

import br.com.senac.dao.ClienteDAO;
import br.com.senac.model.Cliente;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

@Named(value = "clienteBean")
@ViewScoped
public class ClienteBean extends Bean {

    private Cliente cliente;
    private ClienteDAO dao;
    
    private List<String> estados  ; 

    public ClienteBean() {
    }

    @PostConstruct
    public void init() {
        cliente = new Cliente();
        dao = new ClienteDAO();
        estados = new ArrayList<>() ; 
        estados.add("AC");
        estados.add("AL");
        estados.add("AP");
        estados.add("AM");
        estados.add("BA");
        estados.add("CE");
        estados.add("DF");
        estados.add("ES");
        estados.add("GO");
        estados.add("MA");
        estados.add("MT");
        estados.add("MS");
        estados.add("MG");
        estados.add("PA");
        estados.add("PB");
        estados.add("PR");
        estados.add("PE");
        estados.add("PI");
        estados.add("RJ");
        estados.add("RN");
        estados.add("RS");
        estados.add("RO");
        estados.add("RR");
        estados.add("SC");
        estados.add("SP");
        estados.add("SE");
        estados.add("TO");
        
        
        
    }
    
    
    
    

    public void salvar() {

        if (this.cliente.getId() == 0) {
            dao.save(cliente);
            addMessageInfo("Salvo com sucesso.");
        } else {
            dao.update(cliente);
            addMessageInfo("Atualizado com sucesso.");
        }

    }

    public void novo() {
        this.cliente = new Cliente();
    }

    public void deletar(Cliente cliente) {
        this.dao.delete(cliente);
        addMessageInfo("Cliente deletado com sucesso.");

    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public List<String> getEstados() {
        return estados;
    }

    public void setEstados(List<String> estados) {
        this.estados = estados;
    }
    
    

}
