/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.bean;

import br.com.senac.dao.ProdutoDAO;
import br.com.senac.model.Produto;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

/**
 *
 * @author sala304b
 */
@Named(value = "produtoBean")
@ViewScoped
public class ProdutoBean extends Bean {

    private Produto produto;
    private ProdutoDAO dao;

    public ProdutoBean() {
    }

    @PostConstruct
    public void init() {
        this.produto = new Produto();
        this.dao = new ProdutoDAO();

    }

    public void salvar() {

        if (this.produto.getId() == 0) {
            dao.save(produto);
            addMessageInfo("Salvo com sucesso.");
        } else {
            dao.update(produto);
            addMessageInfo("Atualizado com sucesso.");
        }

    }

    public void novo() {
        this.produto = new Produto();
    }

    public void deletar(Produto produto) {
        this.dao.delete(produto);
        addMessageInfo("Produto deletado com sucesso.");

    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

}
