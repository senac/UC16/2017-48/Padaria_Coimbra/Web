
package br.com.senac.dao;

import br.com.senac.model.Cliente;


public class ClienteDAO extends DAO<Cliente>{
    
    public ClienteDAO() {
        super(Cliente.class);
    }
    
}
