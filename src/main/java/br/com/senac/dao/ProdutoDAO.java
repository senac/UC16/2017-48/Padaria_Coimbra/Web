
package br.com.senac.dao;

import br.com.senac.model.Produto;


public class ProdutoDAO extends DAO<Produto> {

    public ProdutoDAO() {
        super(Produto.class);
    }

}
