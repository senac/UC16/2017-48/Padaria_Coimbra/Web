
package br.com.senac.dao;

import br.com.senac.model.Usuario;



public class UsuarioDAO extends DAO <Usuario>{
    
    public UsuarioDAO() {
        super(Usuario.class);
    }
    
}
