package br.com.senac.model;

import java.util.List;
import javax.persistence.Entity;

@Entity
public class Produto extends Entidade {

    private String descricao;
    private double valor;
    private String imagem;
    
    

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public String getImagem() {
        return imagem;
    }

    public void setImagem(String imagem) {
        this.imagem = imagem;
    }

  
    
}
